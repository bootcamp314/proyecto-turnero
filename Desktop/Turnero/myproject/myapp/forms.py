from django import forms
from .models import Client


class ClientModelForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ["ci_client", "name_client", "lastname_client", "type_priority", "type_service"]
        widgets = {
            'ci_client': forms.TextInput(attrs={'class': 'form-control'}),
            'name_client': forms.TextInput(attrs={'class': 'form-control'}),
            'lastname_client': forms.TextInput(attrs={'class': 'form-control'}),
            'type_priority': forms.Select(attrs={'class': 'form-control-sm'}),
            'type_service': forms.Select(attrs={'class': 'form-control-sm'}),
            'ticket_client': forms.TextInput(attrs={'class': 'form-control-sm'}),
            'turn_aux': forms.TextInput(attrs={'class': 'form-control'}),
        }
        
        CHOICES_PRIORITY = [
            ('LOW', 'Low'),
            ('MEDIUM', 'Medium'),
            ('HIGH', 'High'),
        ]
        CHOICES_SERVICE = [
            ('SERVICE A', 'Service A'),
            ('SERVICE B', 'Service B'),
            ('SERVICE C', 'Service C'),
        ]
        labels = {
            'type_priority': 'Priority',
            'type_service': 'Service Type',
        }

        widgets = {
            'type_priority': forms.Select(choices=CHOICES_PRIORITY, attrs={'class': 'form-control'}),
            'type_service': forms.Select(choices=CHOICES_SERVICE, attrs={'class': 'form-control'}),
        }