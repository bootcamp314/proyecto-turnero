from django.db import models

# Create your models here.

class Client(models.Model):
    id              =  models.AutoField(primary_key=True)
    ci_client       = models.CharField(null=True,blank=False,max_length = 100)
    type_priority   = models.CharField(null=False , blank=False) 
    type_service    = models.CharField(null=False , blank=False )
    ticket_client   = models.CharField(null = True, blank=False,max_length = 100)
    turn_aux        = models.IntegerField(null=False , blank=False) 
    name_client     = models.CharField(null = True,blank=False,max_length = 100)
    lastname_client = models.CharField(null = True, blank=False,max_length = 100)
    date_client     = models.DateTimeField(null=True)

    
    class Meta:
        unique_together = ['ci_client']
        indexes = [
            models.Index(fields=['ci_client'])
        ]

    def __str__(self):
        return f'{self.name_client} {self.lastname_client}'

    @staticmethod
    def get_by_ci_client(ci_client):
        try:
            return Client.objects.get(ci_client=ci_client)
        except Client.DoesNotExist:
            return None

#3ra Edad, Embarazadas, Discapacidad =>   Alta
#Clientes corporativos               =>   Media
#Clientes personales                 =>   Baja



    