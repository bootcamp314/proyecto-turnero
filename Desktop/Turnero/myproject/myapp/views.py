from django.shortcuts import render
from .models import Client
from django.shortcuts import render, get_object_or_404, redirect
from .forms import ClientModelForm


# Create your views here.

def Index(request):
    
    return render(request,"sigin.html")


def Verification_User(request): # Verificamos si el usuario es Mariano y la contrasena es 12345.
    
    admin = request.POST['admin']
    password = request.POST['contra']
    clients = Client.objects.all()
    context = { "clients":clients }
    
    if admin == "Mariano" and password == "12345": 
        return render(request,"clientwaiting.html",context)
    
    else:
        return render(request,"error_password.html")
    
    
def ViewClients(request): # Obtenemos la lista de los clientes en la cola de espera 
    
    clients = Client.objects.all()
    context = { "clients":clients }
    
    return render(request, "clientwaiting.html", context)



def ChargeClients(request): # Cargamos los clientes con sus datos a la cola de espera, con los siguientes pasos.
    if request.method == 'POST':
        form = ClientModelForm(request.POST)
        if form.is_valid():
            # Obtenemos los datos del formulario y modificarlos
            
            ci_client = form.cleaned_data['ci_client']
            name_client = form.cleaned_data['name_client']
            lastname_client = form.cleaned_data['lastname_client']
            type_priority = form.cleaned_data['type_priority']
            type_service = form.cleaned_data['type_service']
            turn_aux = 1
            
            # Modificamos los campos manualmente (lo utilizamos para generar tickets y gestionar turnos).
            client_ids = sorted([client.id for client in Client.objects.all()])
            
            if (len(client_ids) == 0):
                aux = 1
            else:
                aux = client_ids[len(client_ids) - 1]
            idaux = aux   
            ticket_client = type_service + " " +  str(idaux)
           
            # Creamos el objeto Client con los datos modificados
            client = Client(ci_client=ci_client, name_client=name_client, 
                            lastname_client=lastname_client, type_priority=type_priority, 
                            type_service=type_service, ticket_client = ticket_client, turn_aux=turn_aux)
            
            # Guardar el cliente ya con el ticket y el turno generado en la base de datos
            client.save()
            return redirect('ViewClients')
    else:
        form = ClientModelForm()

    return render(request, 'charge_clients.html', {'form': form})
    
    
def Delete(request,id): # Recibimos el id del usuario a ser eliminado y lo eliminamos de la base de datos
    
    eliminate_is = Client.objects.get(pk = id)
    eliminate_is.delete() 
    clients = Client.objects.all()        
    context = { "clients":clients }
    
    
    return render(request, "clientwaiting.html", context)


def Edit(request, id): # De la misma forma que el anterior, recibimos el id del usuarioa a ser modificado.
    # Obtener el objeto del cliente a editar, o lanzar un error 404 si no existe
    client = get_object_or_404(Client, pk=id)

    # Crear un formulario con los datos del cliente a editar
    form = ClientModelForm(request.POST or None, instance=client)

    if form.is_valid():
        # Si el formulario es válido, guardardamoslos cambios en la base de datos y redirigirimos a la página de clientes
        form.save()
        return redirect('ViewClients')
        

    
    return render(request, 'client_edit.html', {'form': form, 'client': client})
