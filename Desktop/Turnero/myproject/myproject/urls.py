
from django.conf import settings
from django.contrib import admin
from django.urls import path
from myapp import views
from django.conf.urls.static import static

urlpatterns = [
    
     path('admin/', admin.site.urls),
     path('',views.Index, name='Index'),
     path('view/',views.ViewClients, name='ViewClients'),
     path('delete/<int:id>',views.Delete, name='DeleteClient'),
     path('edit/<int:id>',views.Edit, name='EditClient'),
     path('verific/',views.Verification_User, name='VerificUser'),
     path('charge/',views.ChargeClients, name='ChargeClients'),

]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

